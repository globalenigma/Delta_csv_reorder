#!/usr/bin/python3.5

import argparse
import csv
import sys
import textwrap

data = 'Date,Type,Exchange,Base amount,Base currency,Quote amount,Quote currency,Fee,Fee currency,Costs/Proceeds,Costs/Proceeds currency,Sync Holdings,Sent/Received from,Sent to,Notes\n'\
       '2018-01-01 20:40:00 +01:00,DEPOSIT,Coinbase,10000.00,EUR,,,,,,,,,,Example of fiat deposit\n'\
       '2018-01-08 17:58:00 +01:00,BUY,Coinbase,2.00,BTC,10000.00,EUR,0.10,BTC,,,1,,,1.9 BTC will be added. 10000 EUR will be deducted.\n'\
       '2018-01-10 18:00:00 +01:00,TRANSFER,,1.90,BTC,,,0.01,BTC,,,,Coinbase,GDAX,Example of TRANSFER. Only 0.01 will be deducted.\n'\
       '2018-01-25 15:05:00 +01:00,BUY,GDAX,20.00,ETH,1.00,BTC,0.02,BTC,,,1,,,20 ETH will be added and 1.02 BTC deducted.\n'\
       '2018-02-28 23:59:00 +01:00,SELL,GDAX,10.00,ETH,1.05,BTC,0.50,ETH,,,1,,,10.5 ETH will be deducted and 1.05 BTC added.\n'\
       '2018-03-01 18:22:00 +01:00,BUY,,50.00,SAM (Sample Token Name),1.00,ETH,,,300.00,EUR,1,ICO,,Example of how to add an ICO. 1 ETH will be deducted.\n'\
       '2018-03-28 22:22:00 +02:00,WITHDRAW,,8.50,ETH,,,,,,,,GDAX,OTHER,Example of WITHDRAW. 8.5 ETH will be deducted.\n'\

def sample_data(data):
    print('\n\nSample Data:\n')
    print(data)


def re_order(in_file,out_file):

    # create new column ording using following list:
    fieldnames = ['Date', 'Type', 'Exchange', 'Base amount', 'Base currency', \
                  'Quote amount', 'Quote currency', 'Fee', 'Fee currency', \
                  'Costs/Proceeds', 'Costs/Proceeds currency', 'Sync Holdings', \
                  'Sent/Received from', 'Sent to', 'Notes']

    # Mandatory fields
    mandatory = ['Date', 'Type', 'Base amount', 'Base currency', \
                 'Quote amount', 'Quote currency', 'Fee', 'Fee currency']

    with open(in_file, 'r') as infile:
        reader = csv.DictReader(infile)
        headers = reader.fieldnames
        # Test if file has Mandatory fields needed
        for i in mandatory:
            try: headers.index(i)
            except ValueError as err:
                print("[!] Mandatory field: {0}".format(err))
                error = err
                
        # Test file for any invalid colums
        for i in headers:
            try: fieldnames.index(i)
            except ValueError as err:
                print("[!] Invalid field found: {0}".format(err))
                error = err
    
    with open(in_file, 'r') as infile, open(out_file, 'a') as outfile:
        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
        # Need to re-order the header
        writer.writeheader()
        for row in csv.DictReader(infile):
            # Write newly reordered rows to the new file
            writer.writerow(row)

def main():
    if args.sample:
        sample_data(data)
    if args.infile:
        re_order(args.infile,args.outfile)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
            Simple little csv reorder program for Delta cryptocurrency portfolio tracker.
            Program is used to re-order csv files to work with Delta Crypto import CSV function
            -----------------------------------------------------------------------------------
            If this program was useful for you please consider donating:
              BTC: 3PPAuaEox1oS5yk5bQxCC6qHhbaiW3ngKi
              ETH: 0x6A0090EFDF0aEE384e7b81dC1711A0AB58E8674c
              LTC: MHW14aYrRcbxZysa6ffsuQwzWGHiChY9UT
              ZEN: t1Zw6UWEYEdQ2YZ6mTmw7Z13miZGYuHUR2e
    .
    '''))
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-i', '--infile', required=False, help='input file to do reorder things with')
    group.add_argument('-s', '--sample', required=False, action='store_true', help='print sample data from Delta help spreadsheet')
    parser.add_argument('-o', '--outfile', required=False, default='ordered.csv', help='output file, if not specified it will be ordered.csv')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.0')
    args = parser.parse_args()
    main()